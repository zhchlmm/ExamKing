using ExamKing.Application.Quartzs;
using Furion;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace ExamKing.Application.Quartz
{
    /// <summary>
    /// Quartz Startup
    /// </summary>
    [AppStartup(650)]
    public class QuartzStartup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            //注册调度器工厂
            services.AddSingleton<ISchedulerFactory, StdSchedulerFactory>();
            services.AddSingleton<IJobFactory, ExamJobFactory>();
            //注册调度器工厂
            services.AddSingleton<JobRunner>();
            // 注册开始考试任务
            services.AddScoped<StartExamJob>();
            // 注册结束考试任务
            services.AddScoped<FinshExamJob>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
        }
    }
}