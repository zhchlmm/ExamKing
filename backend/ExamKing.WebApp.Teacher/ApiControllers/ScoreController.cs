using ExamKing.Application.Mappers;
using ExamKing.Application.Services;
using Mapster;
using Microsoft.AspNetCore.Mvc;

namespace ExamKing.WebApp.Teacher
{
    /// <summary>
    /// 成绩接口
    /// </summary>
    public class ScoreController : ApiControllerBase
    {

        private readonly IStuscoreService _stuscoreService;

        public ScoreController(IStuscoreService stuscoreService)
        {
            _stuscoreService = stuscoreService;
        }

        public async Task<PagedList<StuscoreDto>> GetExamScoreList(
            [FromQuery] int examId,
            [FromQuery] int pageIndex = 1,
            [FromQuery] int pageSize = 10
        )
        {
            var examScores = await _stuscoreService.FindExamScoreListByPage(examId, pageIndex, pageSize);
            return examScores;
        }
    }
}