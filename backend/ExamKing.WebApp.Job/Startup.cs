
using ExamKing.Core.Providers;

namespace ExamKing.WebApp.Job
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddViewEngine()
                .AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}