// prod
export const apiUrl = "http://localhost/student-api/v1"
export const hostUrl = "http://localhost"
export const staticUrl = "http://localhost"

// dev
// export const apiUrl = "http://localhost:4001/api/v1"
// export const hostUrl = "http://localhost:4001"
// export const staticUrl = "http://localhost:4001"